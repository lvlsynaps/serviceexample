package com.example.sbt_voronezhtsev_in.servicesample;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sbt-voronezhtsev-in on 25.06.2018.
 */

public class MyService extends Service {

    private static final int MODE = START_NOT_STICKY;

    public static final int MSG_REGISTER_CLIENT = 1;
    public static final int MSG_UNREGISTER_CLIENT = 2;
    public static final String DATA_KEY = "MyService";

    private List<Messenger> mClients = new ArrayList<>();
    private Messenger mMessenger = new Messenger(new IncomingHandler());

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //super.onStartCommand(intent, flags, startId)
        new Thread(new Runnable() {
            @Override
            public void run() {
                int count = 0;
                while (!Thread.interrupted()) {
                    try {
                        Thread.sleep(1000l);
                        count++;
                        Message msg = new Message();
                        Bundle bundle = new Bundle();
                        bundle.putString(DATA_KEY, "Text " + count);
                        msg.setData(bundle);
                        for(Messenger messenger : mClients) {
                            messenger.send(msg);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        return MODE;
    }

    public static Intent newIntent(Context context) {
        return new Intent(context, MyService.class);
    }

    public class IncomingHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;
            }
        }
    }
}
